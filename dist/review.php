<?php include('header.php');?> 
<div class="container">
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Отзывы</li>
  </ol>
</nav>
</div>
</div>
<div class="title">
<div class="container">
        <h1>Отзывы</h1>
    </div>
</div>
<div class="review-content">
    <div class="container">
        <div class="review-card">
        <div class="row">
            <div class="col-xl-2 col-md-3">
                <div class="review-image">
                    <img src="images/dhl.png" alt="">
                </div>
            </div>
            <div class="col-xl-10 col-md-8">
                <div class="review-txt">
                    <div class="review-info">
                        <h2>«DHL Logistic» Миклош Поти</h2>
                        <span>03.12.2019</span>
                    </div>
                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                </div>
            </div>
        </div>
        </div>
        <div class="review-card">
        <div class="row">
            <div class="col-xl-2 col-md-3">
                <div class="review-image">
                    <img src="images/dhl.png" alt="">
                </div>
            </div>
            <div class="col-xl-10 col-md-8">
                <div class="review-txt">
                    <div class="review-info">
                        <h2>«DHL Logistic» Миклош Поти</h2>
                        <span>03.12.2019</span>
                    </div>
                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                </div>
            </div>
        </div>
        </div>
        <div class="review-card">
        <div class="row">
            <div class="col-xl-2 col-md-3">
                <div class="review-image">
                    <img src="images/dhl.png" alt="">
                </div>
            </div>
            <div class="col-xl-10 col-md-8">
                <div class="review-txt">
                    <div class="review-info">
                        <h2>«DHL Logistic» Миклош Поти</h2>
                        <span>03.12.2019</span>
                    </div>
                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                </div>
            </div>
        </div>
        </div>
        <div class="review-card">
        <div class="row">
            <div class="col-xl-2 col-md-3">
                <div class="review-image">
                    <img src="images/dhl.png" alt="">
                </div>
            </div>
            <div class="col-xl-10 col-md-8">
                <div class="review-txt">
                    <div class="review-info">
                        <h2>«DHL Logistic» Миклош Поти</h2>
                        <span>03.12.2019</span>
                    </div>
                    <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                </div>
            </div>
        </div>
        </div>
        <div class="review-btn">
            <button>Оставить отзыв</button>
        </div>
        <div class="review-item">
        <div class="row">
        <div class="col-xl-6">
        <div class="portfolio-btn">
        <div class="pages-inner">
       <a href=""> Назад </a>
    <div class="pages">
                            <input type="radio" id="radio-page" name="radios-one" value="all" checked>
                            <label for="radio-page">1</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-2" name="radios-one" value="all" checked>
                            <label for="radio-page-2">2</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-3" name="radios-one" value="all" checked>
                            <label for="radio-page-3">3</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-4" name="radios-one" value="all" checked>
                            <label for="radio-page-4">4</label>
                        </div>
                        <a href=""> Вперед </a>
    </div>
    </div>
    </div>
    <div class="col-xl-6">
        <div class="all-review">
            <span>Всего 18 Отзывов</span>
        </div>
    </div>
    </div>
    </div>
    </div>
</div>
<?php include('footer.php');?>    