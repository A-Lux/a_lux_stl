 <section class="footer">
   		<div class="container">
   			<div class="row justify-content-between">
   				<div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot ">
   					<a href="#"><img src="images/%D0%9B%D0%BE%D0%B3%D0%BE%20%D0%B8%D0%B7%20%D1%84%D1%83%D1%82%D0%B5%D1%80%D0%B0.png" class='footer-img'></a>
   					<a href="#" style="text-decoration: none">Наши реквизиты</a>
   					<a href="#" style="text-decoration: none">Карта сайта</a>
   				</div>
   				<div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot">
   				    <h2>Наши услуги</h2>
   				    <a href="#" style="text-decoration: none">Железнодорожные перевозки</a>
   				    <a href="#" style="text-decoration: none">Авиаперевозки</a>
   				    <a href="#"  style="text-decoration: none">Автомобильные перевозки</a>
   				    <a href="#" style="text-decoration: none">Мультимодальные перевозки</a>
   				    <a href="#" style="text-decoration: none">Проектные грузы</a>
   				    <a href="#" style="text-decoration: none">Таможенное оформление</a>
   				</div>
   				<div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot">
   				    <h2>Разделы </h2>
   				    <a href="#" style="text-decoration: none">Главная</a>
   				    <a href="#" style="text-decoration: none">О компании</a>
   				    <a href="#" style="text-decoration: none">Отзывы клиентов</a>
   				    <a href="#" style="text-decoration: none">Обращение к клиентам</a>
   				    <a href="#" style="text-decoration: none">Примеры перевозок</a>
   				    <a href="#" style="text-decoration: none">Контакты</a>
   				</div>
   				<div class="col-6 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot">
   				     <h2>Контакты </h2>
   				    <p>Адрес: Алматы 050060 пр.Аль-Фараби 97, офис 54</p>
   				    <p>+7 727 266 80 21,</p>
   				    <p>+7 727 266 80 23</p>
   				    <p>info@gmail.com</p>
   				</div>
   			</div>
   		</div>
   </section>
   <section class="bottom-footer">
       <div class="container">
           <div class="bottom-foot-content">
               <p>2019 STL. Все права защищены. Компания «STL» осуществляет грузовые перевозки по Казахстану и СНГ, доставку грузов из Европы, Азии и любой другой точки мира!</p>
           </div>
       </div>
   </section>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="slick/slick.js"></script>
	<script type="text/javascript" src="slick/slick.min.js"></script>
	<script src="main.js"></script>
</body>
</html>