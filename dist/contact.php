<?php include('header.php');?>   
<div class="container">
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Контакты</li>
  </ol>
</nav>
</div>
</div>
<div class="title">
<div class="container">
        <h1>Контакты</h1>
    </div>
</div>
<div class="contact-page">
    <div class="contact-image">
        <img src="images/service6.png" alt="">
    </div>
<div class="maps">
<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A2aa1043aa5a7aebf0846d18dc5a96b49db6be8a4cec46d44050fab8ceb5ed6b8&amp;source=constructor" width="100%" height="650" frameborder="0"></iframe>
</div> 
<div class="maps-modal">
    <h1>STL LOGISTICS</h1>
    <div class="maps-modal-link">
        <img src='images/maps1.png'>
        <p>Казахстан, г.Алматы 050060
        пр.Аль-Фараби 97, офис 54</p>
    </div>
  
    <div class="maps-modal-link">
    <img src='images/maps2.png'>
        <p class="email-p">info@stl.kz </p>
    </div>
    <div class="maps-modal-link">
    <img src='images/maps3.png'>
    <div class="contact-info">
        <p>Мобильный </p>
        <span>+7 727 266 80 21</span>
        <p>Мобильный </p>
        <span>+7 727 266 80 23 <img src="images/whatsapp.png" alt=""> <img src="images/viber.png" alt=""></span>
        </div>
    </div>
    <div class="maps-modal-link">
        <button>Заказать обратный звонок</button>
    </div>
</div>   
</div>
<div class="company-branches">
    <div class="container">
        <h1>Филиалы компании</h1>
        <div class="branch-inner">
        <div class="row">
            <div class="col-xl-4 col-md-6 col-12">
                <div class="branch-links branch-item ">
                    <div class="branch-image">
                        <img src="images/kz.png" alt="">
                        </div>
                    <div class="branch-content">
                        <h2>KAZAKHSTAN</h2>
                        <p>Soyuztranslink,LLC, Kazakhstan,</p>
                        <p>Almaty, 050059,</p>
                        <p> Eskenderova str..</p>
                        <p>tel.: <b> +7 727 266 80 21</b></p>
                        <p>fax: <b>  +7 727 266 80 21</b></p>
                        <p>e-mail: <b>   info@stl.kz</b></p>
                    </div>
                </div>
                </div>
            <div class="col-xl-4 col-md-6 col-12">
            <div class="branch-links branch-item br-content">
                    <div class="branch-image">
                        <img src="images/uzb.png" alt="">
                        </div>
                    <div class="branch-content">
                        <h2>UZBEKISTAN</h2>
                        <p>Logistics, Ltd., Uzbekistan,</p>
                        <p>Tashkent, 100015,</p>
                        <p> 40 Chekhov str., office 68</p>
                        <p>tel.: <b> +9 9871 150 36 83</b></p>
                        <p>fax: <b>  +9 9871 150 36 83</b></p>
                        <p>e-mail: <b>   info@stl.kz</b></p>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6 col-12">
            <div class="branch-links br-content">
                    <div class="branch-image">
                        <img src="images/krg.png" alt="">
                        </div>
                    <div class="branch-content">
                        <h2>KYRGYZSTAN</h2>
                        <p>Soyuztranslink, LLC,</p>
                        <p>Kyrgyzstan, Bishkek</p>
                        <p>tel.: <b> +9 96 555 730 730</b></p>
                        <p>fax: <b>  +9 96 708 730 730</b></p>
                        <p>e-mail: <b>   bishkek@stl.kz</b></p>
                    </div>
                </div>
            </div>
       
            <div class="col-xl-4 col-md-6 col-12">
                <div class="branch-links branch-item ">
                    <div class="branch-image">
                        <img src="images/belgium.png" alt="">
                        </div>
                    <div class="branch-content">
                        <h2>BELGIUM</h2>
                        <p>Rue Saint Lambert, 51</p>
                        <p>1200 Woluwe Brussels Belgium</p>
                        <p>tel.: <b> +324847368681</b></p>
                        <p>fax: <b>  +32474655450</b></p>
                        <p>e-mail: <b>  ey@stl.kz, ns@stl.kz</b></p>
                    </div>
                </div>
                </div>
        </div>
    </div>
</div>
</div>
</div>

<?php include('footer.php');?>    