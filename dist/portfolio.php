<?php include('header.php');?> 
<div class="portfolio-title">
<div class="container">
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Примеры перевозок</li>
  </ol>
</nav>
</div>
</div>
</div>
<div class="portfolio-content title">
    <div class="container">
        <h1>Примеры перевозок</h1>
        <div class="portfolio-card">
        <div class="row">
            <div class="col-xl-6">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
            </div>
            <div class="col-xl-6">
            <div class="portfolio-txt">
                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus!</h2>
            <h3>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает</h3>
            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
        <p>Команда выехала на объект, чтобы развеять опасения, но они оказались </p>
        <button>Заказать услугу</button>
    </div>
            </div>
            
        </div>
        </div>
        <div class="portfolio-card">
        <div class="row">
            <div class="col-xl-6">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
            </div>
            <div class="col-xl-6">
            <div class="portfolio-txt">
                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus!</h2>
            <h3>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает</h3>
            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
        <p>Команда выехала на объект, чтобы развеять опасения, но они оказались </p>
        <button>Заказать услугу</button>
    </div>
            </div>
            
        </div>
        </div>
        <div class="portfolio-card">
        <div class="row">
            <div class="col-xl-6">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
            </div>
            <div class="col-xl-6">
            <div class="portfolio-txt">
                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus!</h2>
            <h3>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает</h3>
            <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
        <p>Команда выехала на объект, чтобы развеять опасения, но они оказались </p>
        <button>Заказать услугу</button>
    </div>
            </div>
            
        </div>
    </div>
    <div class="portfolio-btn">
        <div class="pages-inner">
       <a href=""> Назад </a>
    <div class="pages">
                            <input type="radio" id="radio-page" name="radios-one" value="all" checked>
                            <label for="radio-page">1</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-2" name="radios-one" value="all" checked>
                            <label for="radio-page-2">2</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-3" name="radios-one" value="all" checked>
                            <label for="radio-page-3">3</label>
                        </div>
                        <div class="pages">
                            <input type="radio" id="radio-page-4" name="radios-one" value="all" checked>
                            <label for="radio-page-4">4</label>
                        </div>
                        <a href=""> Вперед </a>
    </div>
    </div>
</div>
<?php include('footer.php');?>    