$(document).ready(function () {
  // SELECT // $('.select2').select2({
  //   minimumResultsForSearch: -1
  // });

 

  // SELECT
 $('.link-overflow').click(function(e){
      e.preventDefault();
      $('.service-info').toggleClass('service-info-active');
      console.log('overflow')
});

  $('.company-item').click(function(e){
    console.log('link')
    let link = $(this).attr('href');
    let top = $(link).offset().top;

    $('body,html').animate({scrollTop: top}, 1000);
  });

  var hash = location.hash;
  if($(hash).length){
      var top = $(hash).offset().top -50;
      $('body,html').animate({scrollTop: top}, 1000);
  }




  $('.burger-menu-btn').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
  });
  $('.transport-slider').slick({
    infinite: true,
    autoplay: true,
    slidesToShow: 1,
    adaptiveHeight: true
  });
  // $('.slider-base').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   fade: true,
  //   asNavFor: '.slider-list'
  // });
  // $('.slider-list').slick({
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   asNavFor: '.slider-base',
  //   centerMode: true,
  //   focusOnSelect: true
  // });

  $('.slider-base').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-list'
  });
  $('.slider-list').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-base',
    dots: true,
    centerMode: true,
    focusOnSelect: true
  });

  $('.partners-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true
  });
  $('.gallery-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  });
   $('.company-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  });
    $('.clients-company').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.certificates-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});
