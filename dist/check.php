<?php include('header.php');?>   
<div class="check-page">
    <div class="container" >
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Проверка на благонадежность</li>
  </ol>
</nav>
</div>
    </div>
    <div class="check-page-content">
    <div class="container">
    <div class="crumb-check">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs-check">
    <li class="breadcrumb-item check-item "><a href="#">Due Deligence</a></li>
    <li class="breadcrumb-item check-crumb" aria-current="page">проверка на благонадежность</li>
  </ol>
</nav>
</div>
</div>
    </div>
</div>
<div class="check-image">

</div>
<div class="check-txt">
    <div class="container">
        <h1>
        Одно из новых направлений для нашей компании
        </h1>
        <h3>Этот сервис появился по многочисленным просьбам наших клиентов:</h3>
        <p>Бизнесмены из других стран которые бы хотели начать сотрудничество с компаниями из <b>Европы, Китая, Центральной Азии</b> порой нашли контакты предполагаемых поставщиков на интернете.</p>
        <p>У них нет возможности поехать и проверить есть ли что то за спиной у так называемого поставщика <b>(особенно касается мелких предприятий лиьо торговых компаний)</b> кроме красивого сайта.</p>
        <p>В таких случая наши представители могут по поручению клиента посетить офис или склад или завод клиента, предоставить фотографии, проверить налоговый сайт страны и так далее.</p>
    <p>Это защитит клиента от потери денег <b>(при предоплате, акредетиве)</b> и от потери репутации в случае если клиент перепродает товар который он планирует купить.</p>
    </div>
  
</div>
<div class="check-link">
        <div class="container">
    <div class="row">
        <div class="col-xl-4 col-md-4 col-lg-4">
            <div class="check-link-img">
                <img src="images/img-check.png" alt="">
            </div>
        </div>
        <div class="col-xl-8 col-md-8 col-lg-8">
            <div class="check-link-txt">
                <h1>Гарантия сохранности денег</h1>
                <p>Так же возможно проводить опату через наши офисы, в этом случае мы отдаем оплату продавцу в момент забора груза - это гарантирует так же сохранность денег</p>
            </div>
        </div>
    </div>
    </div>
    </div>
<?php include('footer.php');?>    