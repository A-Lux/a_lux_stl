<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <title>Дизайн корпоративного портала-'перевозки-STL'</title>
    <link rel="stylesheet" href="style/all.css">
    <link rel="stylesheet" href="style/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    
    <section class="head-desc">
    <div class="header-desc">
     <div class="container">

         <div class="row">
             <div class="col-xl-1">
                 <div class="logo-desc">
                     <img src="images/%D0%9B%D0%BE%D0%B3%D0%BE%20%D0%B8%D0%B7%20%D1%88%D0%B0%D0%BF%D0%BA%D0%B8.png" alt="">
                 </div>
             </div>
             <div class="col-xl-11">
                 <div class="top-header-desc">
                     <ul class="nav">
      <li class="nav-item phone">
        <a class="nav-link top-link header-link" href="#"><img src="icons/phone%2018px.svg">+7 (727) 266 80 21</a>
      </li>
      <li class="nav-item email" >
        <a class="nav-link top-link header-link" href="#"><img src="icons/Group.png">  info@stl.kz</a>
      </li>
      <li class="nav-item skype">
        <a class="nav-link top-link header-link" href="#"> <img src="icons/skype%2018px.svg">  Reseptionstl</a>
      </li>
      <button class="head-btn">Рассчитать</button>
      <button class="head-btn">Скачать брошюру</button>
      <button class="head-btn">презентация</button>
      <div class="dropdown skills-desc">
    <a class="btn dropdown-toggle skills-desc-btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    ENG <img src="icons/skills-btn.png" alt="">
  </a>

  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
    <a class="dropdown-item" href="#">Action</a>
    <a class="dropdown-item" href="#">Another action</a>
    <a class="dropdown-item" href="#">Something else here</a>
  </div>
</div>
      </ul>
                 </div>
             </div>
             <div class="burger-menu">
		<a href="#" class="burger-menu-btn">
			<span class="burger-menu-lines"></span>
		</a>
			</div>

	<div class="nav-panel-mobil">
		<div class="container">
			<nav class="navbar-expand-lg navbar-light">
				<ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
					<li class="nav-item">
						<a class="nav-link" href="#">О компании</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Наши услуги</a>
					</li>
					 <li class="nav-item">
            <a class="nav-link item icons" href="#">Примеры перевозок</a>
         </li>
         <li class="nav-item">
            <a class="nav-link item icons"  href="#">Контакты</a>
         </li>
         <li class="nav-item">
         <button class="head-btn">Рассчитать</button>
                    </li>
                    <li class="nav-item">
      <button class="head-btn">Скачать брошюру</button>
                    </li>
                    <li class="nav-item">
      <button class="head-btn">презентация</button>
                    </li>
    <li class="nav-item phone">
        <a class="nav-link" href="#"><img src="icons/phone%2018px.svg">+7 (727) 266 80 21</a>
      </li>
      <li class="nav-item email" >
        <a class="nav-link" href="#"><img src="icons/Group.png">  info@stl.kz</a>
      </li>
      <li class="nav-item skype">
        <a class="nav-link" href="#"> <img src="icons/skype%2018px.svg">  Reseptionstl</a>
      </li>
    
				</ul>

			</nav>
		</div>
	</div>
</section>

    