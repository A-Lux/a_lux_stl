<?php include('header.php');?>   
<div class="container">
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Услуги</li>
  </ol>
</nav>
</div>
</div>
<div class="title">
<div class="container">
        <h1>Услуги</h1>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Автомобильные перевозки</h1>
                    <h3>STL предложит Вам лучшие условия обслуживания, лучшее качество работы и конкурентоспособные цены</h3>
                    <ul>
                        <li> 	&emsp; Различные виды транспортных средств (открытые площадки, специальные подвижные составы для негабаритных грузов, рефрижераторы)</li>
                        <li> 	&emsp; Авто со спец разрешением для перевозки ADR (опасных грузов)</li>
                        <li>  	&emsp; Консолидация грузов из любой точки Европы в Казахстан</li>
                        <li>  	&emsp; При консолидации на нашем складе в Европе, мы можем предоставить независимого сюрвейера с оформлением Акта и Фото о загрузке</li>
                        <li> 	&emsp; Проверка груза осуществляется в месте загрузки и контролируется до пункта конечного назначения</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p> <b>Автоперевозки достаточно популярны благодаря своей доступности большому кругу клиентов </b> – начиная крупными предприятиями, которым требуется транспортировка негабаритного груза, и заканчивая обычными людьми, которые заказали товар в интернет-магазине.</p>
                    <p> <b>Автоперевозки достаточно популярны благодаря своей доступности большому кругу клиентов </b> – начиная крупными предприятиями, которым требуется транспортировка негабаритного груза, и заканчивая обычными людьми, которые заказали товар в интернет-магазине.</p>
                    <p> <b>Автоперевозки достаточно популярны благодаря своей доступности большому кругу клиентов </b> – начиная крупными предприятиями, которым требуется транспортировка негабаритного груза, и заканчивая обычными людьми, которые заказали товар в интернет-магазине.</p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Авиа грузоперевозки</h1>
                    <h3>Мы обеспечим доставку любого авиагруза, из любой точки мира.</h3>
                    <h3>Большой опыт работы в сфере Авиаперевозок и широкая сеть агентов в главных аэропортах мира, позволяет компании STL доставлять авиагрузы</h3>
                    <ul>
                        <li> 	&emsp; «Точно-в-срок», без повреждений.</li>
                        <p> Мы предлагаем вам самый полный список локальных и международных услуг:</p>
                        <li> 	&emsp; Доставка грузов в самые короткие сроки “от двери до двери”</li>
                        <li>  	&emsp; Авиа перевозка негабаритных и тяжеловесных грузов</li>
                        <li>  	&emsp; Чартерные перевозки в разных направлениях по всему миру</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p> В месте назначения мы принимаем груз и перераспределяем, распределяем в пределах страны или в соседний регион.</p>
                    <p> Самым оперативным способом отправки груза по праву считается доставка воздушным путем. Это особенно ощутимо в глобальном масштабе, поскольку международные грузовые перевозки, как правило, подразумевают транспортировку груза на дальние расстояния. Поэтому, если Вам необходимо доставить груз из одной точки мира в другую за </p>
                    <p> В месте назначения мы принимаем груз и перераспределяем, распределяем в пределах страны или в соседний регион.</p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Железнодорожные перевозки</h1>
                    <h3>Компания STL имеет собственные железнодорожные коды в Казахстанe.</h3>
                    <ul>
                        <li> 	&emsp; Терминальные услуги</li>
                        <li> 	&emsp; Моментальный расчет тарифов по Казахстану и странам СНГ</li>
                        <li>  	&emsp; Расчет негабаритных и сверхгабаритных грузов</li>
                        <li>  	&emsp;  Регулярная информация о местонахождении Вашего груза и времени прибытия в пункт конечного назначения</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p> <b>Железнодорожные грузоперевозки из Алматы, Астаны и других городов Казахстана </b> считаются одним из самых оперативных и надежных способов транспортировки груза. Достоинством железнодорожных грузоперевозок является возможность доставки грузов в больших объемах. При этом стоимость транспортировки по сравнению с автомобильными или авиационными перевозками существенно ниже.</p>
                    <p> Независимость от погодных факторов делает железнодорожные грузоперевозки надежнейшим видом транспортировки, который гарантирует полную сохранность предоставленного груза и исключает задержку доставки. Логистическая компания «STL» занимается международными грузовыми перевозками, а также доставкой грузов из Европы, </p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Мультимодальные перевозки</h1>
                    <h3>Наша компания осуществляет международные перевозки, в которых задействованы различные виды транспорта — морской, железнодорожный и автомобильный.</h3>
                    <h3>STL является экспертом в перевозке спец. оборудования: от накатных грузов и негабаритных до стандартных, в том числе в специальных контейнерах:</h3>
                    <ul>
                        <li> 	&emsp;  “Оpen Top” (с открытым верхом), “Flat Rack” (плоская рама для перевозки спец.техники и негабаритных грузов)</li>
                        <li> 	&emsp; Грузов, имеющих определенный температурный режим хранения.</li>
                        <li>  	&emsp;Трубы для нефтяных и газовых объектов, большие объемы стали для строительства, транспортировка целых промышленных установок.</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p> Мы скоординируем перевозку Вашего груза со всех континентов через порты Балтийского, Черного моря и Восточно-Азиатского побережья.</p>
                    <p> Мультимодальные перевозки – это комплексный метод доставки груза, который осуществляется сразу несколькими видами транспортировки – наземным, морским, железнодорожным и т.д. Если груз не получается доставить до пункта назначения с помощью одного способа транспортировки, самым практичным и оперативным вариантом </p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Негабаритные и тяжеловесные перевозки. проектные грузы.</h1>
                    <h3>Наша компания имеет богатый опыт доставки в Казахстан сложного оборудования для нефтегазового сектора, машиностроительной индустрии, горнодобывающей отрасли, сельскохозяйственной отрасли.</h3>
                    <ul>
                        <li> 	&emsp;     Нестандартным, негабаритным, слишком тяжелым,</li>
                        <li> 	&emsp; Или требующим специальных условий перевозки,</li>
                        <li>  	&emsp; Либо просто чрезвычайно срочным.</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p>Негабаритные тяжеловесные грузоперевозки требуют высокого уровня профессионализма, грамотной организации маршрута и слаженной работы специалистов.</p>
                    <p> Грамотно организованная грузоперевозка негабаритов обеспечит своевременную и надежную доставку Вашего груза.</p>
                    <p> Негабаритные тяжеловесные грузоперевозки предназначены для:
дорожно-строительной техники;</p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Таможенные услуги</h1>
                    <h3>Персонал нашей Компании с большим опытом работы в сфере таможенного оформления предоставит Вам квалифицированную консультацию</h3>
                    <ul>
                        <li> 	&emsp; По основным требованиям к товаросопроводительным документам для этспортно -импортного таможенного оформления грузов.</li>
                        <li> 	&emsp; По этапам процесса таможенного оформления грузов</li>
                        <li>  	&emsp; О получении сертификатов соответствия</li>
                        <li>  	&emsp; О получения разрешения КНБ и разрешений других компетентных органов</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p> Транспортировка груза за границу обязательно сопровождается таможенным оформлением. Это скрупулезный процесс, который способен превратиться в настоящую бюрократическую проволочку. Гораздо проще обратиться с этим вопросом к профессионалам, которые сохранят Вам нервы и время. Специалисты транспортной компании подготовят пакет всех необходимых документов, лицензий и сертификатов, рассчитают сумму таможенного платежа и оформят декларацию.</p>
                    <p> Опытные специалисты, компетентные в вопросах логистики и таможенного оформления грузов, учтут все нюансы и разрешат любые ситуации. Обратившись за помощью в компанию «STL», Вы можете быть уверены, что таможенное оформление Вашего груза </p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="services services-bottom">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="services-images">
                <div class="slider-product">
            <div class="slider-base">
            <div class="slides">
                                    <img src="images/slide1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
                                <div class="slides">
                                    <img src="images/service1.png" alt="">
                                </div>
            </div>
            <div class="slider-list">
                                <div class="slider-links"><img src="images/slide2.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide3.png" alt=""></div>
                                <div class="slider-links"><img src="images/slide4.png" alt=""></div>
                            </div>
                </div>
                <div class="services-button">
                <button>Задать вопрос эксперту</button>
                </div>
            </div>
            </div>
            <div class="col-xl-6">
                <div class="service-content">
                    <h1>Проверка на благонадежность</h1>
                    <h3>Одно из новых направлений для нашей компании. Этот сервис появился по многочисленным просьбам наших клиентовконсультацию</h3>
                    <ul>
                        <li> 	&emsp;   Бизнесмены из других стран которые бы хотели начать сотрудничество с компаниями из Европы, Китая, Центральной Азии порой нашли контакты предполагаемых поставщиков на интернете</li>
                    </ul>
                </div>
                <div class="service-info">
                    <p> У них нет возможности поехать и проверить есть ли что то за спиной у так называемого поставщика (особенно касается мелких предприятий лиьо торговых компаний) кроме красивого сайта</p>
                    <p> В таких случая наши представители могут по поручению клиента посетить офис или склад или завод клиента, предоставить фотографии, проверить налоговый сайт страны и так далее</p>
                    <p> Это защитит клиента от потери денег (при предоплате, акредетиве) и от потери репутации в случае если клиент перепродает товар который он планирует купить
 Так же возможно проводить опату через наши офисы, в этом случае мы отдаем оплату </p>
                </div>
                <div class="read-overflow">
                <a href="#" class="link-overflow">Читать далее <img src="images/arrow-read.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include('footer.php');?>   