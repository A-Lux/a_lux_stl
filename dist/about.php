<?php include('header.php');?>   
<div class="container">
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">О компании</li>
  </ol>
</nav>
</div>
</div>
<div class="title">
<div class="container">
        <h1>О компании</h1>
    </div>
</div>
<div class="about-page">
    <div class="container">
        <div class="about-content">
        <div class="row">
            <div class="col-xl-10 col-md-10">
            <div class="about-text">
            <p><b>Компания «STL»</b> осуществляет грузовые перевозки по <b>Казахстану и СНГ</b>, доставку грузов из <b>Европы, Азии и любой другой точки мира.</b> Мы занимаемся грузоперевозками любого объема и уровня сложности. Если Вас интересует перевозка оборудования, строительных материалов и других негабаритных тяжеловесных 
                грузов, смело обращайтесь к нам за помощью!</p>
            </div>
        </div>
        <div class="col-xl-2 col-md-2">
            <div class="about-image">
                <img src="images/about-image.png" alt="">
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
<div class="about-video">
    <div class="container">
        <div class="video-inner">
        <div class="row">
            <div class="col-xl-5">
                <div class="video-content about-title">
                    <h1>Видео о нашей компании</h1>
                    <p>«Мы очень сильно ценим ваше Доверие. Доверие наших клиентов. Которое основано на реальных делах, на реальных перевозках. На реальном ежедневном труде наших сотрудников.»</p>
                    <button>Скачать брошюру</button>
                </div>
            </div>
            <div class="col-xl-7">
                <iframe width="650" height="343" src="https://www.youtube.com/embed/4bK0feUQf-Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
        <div class="partners-info" id="partners-info">
            <div class="partners-info-title">
                <h1>Уважаемые партнеры!</h1>
                <p>13/12/2019</p>
            </div>
            <p>На протяжении уже 14 лет наша компания растет из года в год, и, не смотря ни на что, мы продолжаем свое уверенное развитие. Конечно, это не легко, особенно сейчас, но нас движет вперед наша цель, наше видение, наше понимание для чего мы работаем с вами. С самого начала мы поставили себе цель – стать казахстанской компанией мирового класса, стать лидером на своем рынке, и предоставлять сервис на уровне международных логистических компаний.</p>
            <p>Мы делаем все, чтобы наши клиенты чувствовали, что они получают надежные и качественные услуги, с идеальным балансом цена/качество. Ведь правильно выстроенная логистика может стать базой успеха любого бизнеса, и именно поэтому, мы хотим сделать вашу логистику легкой и надежной, а это гораздо шире, чем просто доставлять грузы.</p>
            <p>Именно для удобства обслуживания экспортных и импортных операций наших клиентов мы открыли наши офисы в Китае, Индии, ОАЭ и в Европе. Именно поэтому мы сознательно инвестируем в обучение и повышение квалификации персонала. Именно поэтому мы ориентируемся на долгосрочность отношений с клиентом, превращение их в партнерские, чем на извлечение сиюминутной выгоды за счет покупателя.</p>
            <p>«Потому, что мы очень сильно ценим ваше Доверие. Доверие наших клиентов. Которое основано на реальных делах, на реальных перевозках. На реальном ежедневном труде наших сотрудников.»</p>
            <p>Выбрав компанию STL в качестве своего логистического поставщика, вы получите не только высококачественный сервис, но и заинтересованного и высокопрофессионального партнера, способного помочь Вам поднять свой бизнес на новый уровень.</p>
            <p> <b>Мы делаем все, чтобы наши клиенты чувствовали, что они получают надежные и качественные услуги, с идеальным балансом цена/качество. Ведь правильно выстроенная логистика может стать базой успеха любого бизнеса, и именно поэтому, мы хотим сделать вашу логистику легкой и надежной, а это гораздо шире, чем просто доставлять грузы.</b></p>
        
        
        </div>
      
</div>
<div class="about-us">
    <div class="container">
    <h1>Почему именно наша компания?</h1>
    <div class="row">
        <div class="col-xl-6">
          <div class="advantages advantages-top">
              <div class="advantages-image">
            <img src="images/about-icon1.png" alt="">
        </div>
        <div class="advantages-text">
            <h1>Быстрый результат </h1>
            <p>Слаженная работа наших сотрудников обеспечивает своевременную доставку грузов в указанный пункт назначения.</p>
        </div>
          </div>  
        </div>
        <div class="col-xl-6">
            <div class="advantages advantages-top">
                <div class="advantages-image">
              <img src="images/about-icon2.png" alt="">
          </div>
          <div class="advantages-text">
              <h1>Профессионализм </h1>
              <p>В нашей Компании работают настоящие профессионалы в области логистики и транспортировки грузов. Более того, мы регулярно инвестируем в обучение и повышения квалификации </p>
          </div>
        </div>
        </div>
        <div class="col-xl-6">
            <div class="advantages">
                <div class="advantages-image">
              <img src="images/about-icon3.png" alt="">
          </div>
          <div class="advantages-text">
              <h1>Гарантированная надежность</h1>
              <p>Высококвалифицированные эксперты в области логистики грамотно оформят любую документацию, связанную с транспортировкой и таможенным оформлением груза. Наши клиенты и партнеры могут не волноваться за целостность и сохранность транспортируемых предметов, поскольку мы осуществляем страхование грузов.</p>
          </div>
        </div>
        </div>
        <div class="col-xl-6">
            <div class="advantages">
                <div class="advantages-image">
              <img src="images/about-icon4.png" alt="">
          </div>
          <div class="advantages-text">
              <h1>Высокое качество</h1>
              <p>Мы гарантируем каждому клиенту индивидуальный подход, поскольку наша Компания ориентирована на долгое и взаимовыгодное сотрудничество.</p>
          </div>
        </div>
        </div>
        <div class="col-xl-12">
            <div class="advantages-btn text-center">
                <button>Получить консультацию</button>
            </div>
        </div>
   </div>
</div>
</div>
<div class="about-company" id="about-review">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-md-6">
                <div class="about-company-text">
                    <h1>Сомневаетесь?</h1>
                    <h1>Так узнайте больше о нашей компании</h1>
                    <p><b>Компания «STL»</b> осуществляет грузовые перевозки по Казахстану и СНГ, доставку грузов из Европы, Азии и любой другой точки мира. Мы занимаемся грузоперевозками любого объема и уровня сложности. </p>
                    <p>Если Вас интересует перевозка оборудования, строительных материалов и других негабаритных тяжеловесных грузов, смело обращайтесь к нам за помощью!</p>
                    <img src="images/arrow.png" alt="">
                </div>
            </div>
            <div class="col-xl-5 p-0 col-md-6">
                <div class="about-company-slider">
                    <div class="company-slider">
                        <div class="stl-slide">
                           <div class='stl-slide-content'>
                            <img src="images/slide-icon.png" alt="">
                            <div class="stl-slide-text">
                                <h1>Хайдельберг</h1>
                                <p>Роман Табачников </p>
                            </div>
                           </div>
                           <p>«С компанией STL мы работаем с мая 2008 года. STL перевезла для нас оборудование для цементной мельницы из Испании в Усть-Каменогорск, насосы «Metso Minerals » из Швеции...</p>
                        </div>
                        <div class="stl-slide">
                            <div class='stl-slide-content'>
                                <img src="images/slide-icon.png" alt="">
                                <div class="stl-slide-text">
                                    <h1>Хайдельберг</h1>
                                    <p>Роман Табачников </p>
                                </div>
                               </div>
                               <p>«С компанией STL мы работаем с мая 2008 года. STL перевезла для нас оборудование для цементной мельницы из Испании в Усть-Каменогорск, насосы «Metso Minerals » из Швеции...</p>
                               
                        </div>
                        <div class="stl-slide">
                            <div class='stl-slide-content'>
                                <img src="images/slide-icon.png" alt="">
                                <div class="stl-slide-text">
                                    <h1>Хайдельберг</h1>
                                    <p>Роман Табачников </p>
                                </div>
                               </div>
                               <p>«С компанией STL мы работаем с мая 2008 года. STL перевезла для нас оборудование для цементной мельницы из Испании в Усть-Каменогорск, насосы «Metso Minerals » из Швеции...</p>
                        </div>
                    </div>
                    <div class="stl-slide-btn">
                        <button>Больше отзывов о нас</button>
                     </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clientsDiv" id="clients-company">
    <div class="container">
        <div class="about-title">
            <h1>Партнёры и клиенты нашей компании </h1>
        </div>
<div class="clients-company" >
           <div class="clients-img">
            <img src='logo/about-logo.png'>
            <img src='logo/about-logo2.png'>
           </div>
        <div class="clients-img">
            <img src='logo/about-logo3.png'>
            <img src='logo/about-logo4.png'>
           </div>
        <div class="clients-img">
            <img src='logo/a-logo3.png'>
            <img src='logo/a-logo.png'>
           </div>
        <div class="clients-img">
            <img src='logo/a-logo7.png'>
            <img src='logo/a-logo8.png'>
           </div>
        <div class="clients-img">
            <img src='logo/a-logo9.png'>
            <img src='logo/about-logo.png'>
           </div>
        <div class="clients-img">
            <img src='logo/a-logo10.png'>
            <img src='logo/about-logo3.png'>
           </div>
    </div>
</div>    
</div>
</div>
<div class="certificates">
    <div class="container">
        <div class="about-title certificate-title">
        <h1>Сертификаты, дипломы,</h1>
        <h1>благодарственные письма:</h1> 
        </div>
        <div class="certificates-slider">
            <div class="certificate">
             <img src="images/certificate.png" alt="">
            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">
            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">
            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">

            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">
            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">
            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">
            </div>
            <div class="certificate">
                <img src="images/certificate.png" alt="">
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?>   