<?php include('header.php');?>   
<div class="container">
        <div class="crumb-top">
    <nav aria-label="breadcrumb ">
  <ol class="breadcrumb crumbs">
    <li class="breadcrumb-item link-active"><a href="index.html">Главная</a></li>
    <li class="breadcrumb-item active" aria-current="page">Отзывы</li>
  </ol>
</nav>
</div>
</div>
<div class="title">
<div class="container">
        <h1>Галерея</h1>
    </div>
</div>
<div class="gallery-content">
    <div class="container">
    <div class="row">
        <div class="col-xl-4 p-0 col-md-6 col-lg-4">
            <div class="gallery-slider">
                <div class="slide-product">
                    <img src="images/service1.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service2.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service3.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-xl-4 p-0 col-md-6 col-lg-4">
        <div class="gallery-slider">
                <div class="slide-product">
                    <img src="images/service2.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service1.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service3.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-xl-4 p-0 col-md-6 col-lg-4">
        <div class="gallery-slider">
                <div class="slide-product">
                    <img src="images/service3.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service2.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service1.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-xl-4 p-0 col-md-6 col-lg-4">
            <div class="gallery-slider">
                <div class="slide-product">
                    <img src="images/service4.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service5.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service1.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-xl-4 p-0 col-md-6 col-lg-4">
            <div class="gallery-slider">
                <div class="slide-product">
                    <img src="images/service5.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service2.png" alt="">
                </div>
                <div class="slide-product">
                    <img src="images/service4.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-xl-4 p-0 col-md-6 col-lg-4">
            <div class="gallery-image">
                <img src="images/service6.png" alt="">
            </div>
            </div>
        </div>
    </div>
    </div>
</div>

<?php include('footer.php');?>    